import _ from 'lodash'
import './fonts/fonts.css'
import './styles/style.css'

import funcPrint from './print'

function component() {
  const component = document.createElement('div')

  const logo = document.createElement('div')
  logo.classList.add('logo')

  const author = document.createElement('p')
  author.innerHTML = 'Edward Chagirov'
  author.classList.add('author')

  const button = document.createElement('button')
  button.innerHTML = 'Click me'
  button.onclick = funcPrint

  component.appendChild(button)
  component.appendChild(logo)
  component.appendChild(author)

  return component
}

document.body.appendChild(component())